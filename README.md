# RobotsRace #

This is the code repository for the [2IV60](http://www.win.tue.nl/~vanwijk/2IV60/) course assignment. This assignment is made by Kasper Rijksen and Gijs Rijnders. It is an OpenGL application that renders a 3D scene with a terrain, a racetrack and a few robots walking over that racetrack.

### What is this repository for? ###

The code for building the assignment can be useful for others. The course material is helpful for learning OpenGL and computer graphics mathematics and basics.