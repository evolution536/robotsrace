package robotrace;

import robotrace.terrain.Terrain;
import robotrace.robot.Robot;
import static java.lang.Math.*;
import static javax.media.opengl.GL2.*;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_LIGHT0;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_NORMALIZE;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_POSITION;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_SMOOTH;

/**
 * Handles all of the RobotRace graphics functionality, which should be extended
 * per the assignment.
 *
 * OpenGL functionality: - Basic commands are called via the gl object; -
 * Utility commands are called via the glu and glut objects;
 *
 * GlobalState: The gs object contains the GlobalState as described in the
 * assignment: - The camera viewpoint angles, phi and theta, are changed
 * interactively by holding the left mouse button and dragging; - The camera
 * view width, vWidth, is changed interactively by holding the right mouse
 * button and dragging upwards or downwards; - The center point can be moved up
 * and down by pressing the 'q' and 'z' keys, forwards and backwards with the
 * 'w' and 's' keys, and left and right with the 'a' and 'd' keys; - Other
 * settings are changed via the menus at the top of the screen.
 *
 * Textures: Place your "track.jpg", "brick.jpg", "head.jpg", and "torso.jpg"
 * files in the same folder as this file. These will then be loaded as the
 * texture objects track, bricks, head, and torso respectively. Be aware, these
 * objects are already defined and cannot be used for other purposes. The
 * texture objects can be used as follows:
 *
 * gl.glColor3f(1f, 1f, 1f); track.bind(gl); gl.glBegin(GL_QUADS);
 * gl.glTexCoord2d(0, 0); gl.glVertex3d(0, 0, 0); gl.glTexCoord2d(1, 0);
 * gl.glVertex3d(1, 0, 0); gl.glTexCoord2d(1, 1); gl.glVertex3d(1, 1, 0);
 * gl.glTexCoord2d(0, 1); gl.glVertex3d(0, 1, 0); gl.glEnd();
 *
 * Note that it is hard or impossible to texture objects drawn with GLUT. Either
 * define the primitives of the object yourself (as seen above) or add
 * additional textured primitives to the GLUT object.
 */
public class RobotRace extends Base {

    /**
     * Array of the four robots.
     */
    private final Robot[] robots;

    /**
     * Instance of the camera.
     */
    private final Camera camera;

    /**
     * Instance of the race track.
     */
    private final RaceTrack[] raceTracks;

    /**
     * Instance of the terrain.
     */
    private final Terrain terrain;

    /**
     * Constructs this robot race by initializing robots, camera, track, and
     * terrain.
     */
    public RobotRace() {
        // Create a new array of four robots
        robots = new Robot[4];

        // Initialize robot 0
        robots[0] = new Robot(new Vector(0, 0, 0), new Vector(0, 0, 0), Material.GOLD, 1.0f);

        // Initialize robot 1
        robots[1] = new Robot(new Vector(0, 0, 0), new Vector(0, 0, 0), Material.SILVER, 1.0f);

        // Initialize robot 2
        robots[2] = new Robot(new Vector(0, 0, 0), new Vector(0, 0, 0), Material.WOOD, 1.0f);

        // Initialize robot 3
        robots[3] = new Robot(new Vector(0, 0, 0), new Vector(0, 0, 0), Material.ORANGE_PLASTIC, 1.0f);

        // Initialize the camera
        camera = new Camera();

        // Initialize the race tracks
        raceTracks = new RaceTrack[5];

        // Test track
        raceTracks[0] = new RaceTrack();

        // Make instance of our BezierHelper class, used to determine control points for bezier splines.
        BezierHelper bezierHelper = new BezierHelper();

        // O-track
        raceTracks[1] = new RaceTrack(bezierHelper.generateEllipsizedTrack(8.0f, 14.5f, 64, 0));

        // L-track
        raceTracks[2] = new RaceTrack(bezierHelper.generateCapitalLtrack());

        // C-track
        raceTracks[3] = new RaceTrack(bezierHelper.generateCapitalCTrack());

        // Custom track
        raceTracks[4] = new RaceTrack(bezierHelper.generateEllipsizedTrack(8.0f, 14.5f, 64, 10));

        // Initialize the terrain
        terrain = new Terrain(40, 40);
    }

    /**
     * Called upon the start of the application. Primarily used to configure
     * OpenGL.
     */
    @Override
    public void initialize() {
        // Enable blending.
        gl.glEnable(GL_BLEND);
        gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // Enable depth testing.
        gl.glEnable(GL_DEPTH_TEST);
        gl.glDepthFunc(GL_LESS);

        // Enable textures. 
        gl.glEnable(GL_TEXTURE_1D);
        gl.glEnable(GL_TEXTURE_2D);
        gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        gl.glBindTexture(GL_TEXTURE_2D, 0);

        // Try to load four textures, add more if you like.
        track = loadTexture("track.jpg");
        brick = loadTexture("brick.jpg");
        head = loadTexture("head.jpg");
        torso = loadTexture("torso.jpg");

        gl.glShadeModel(GL_SMOOTH); // Use smooth shading.

        // Lighting
        gl.glEnable(GL_LIGHTING); // Enable lighting.
        gl.glEnable(GL_LIGHT0); // Enable light source #0.

        // Create light components
        float ambientLight[] = {0.2f, 0.2f, 0.2f, 1.0f};
        float diffuseLight[] = {0.8f, 0.8f, 0.8f, 1.0f};
        float specularLight[] = {0.5f, 0.5f, 0.5f, 1.0f};
        float positionLight[] = {1.0f, 0.0f, 1.0f, 1.0f};

        // Assign created components to GL_LIGHT0
        gl.glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight, 0);
        gl.glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight, 0);
        gl.glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight, 0);
        gl.glLightfv(GL_LIGHT0, GL_POSITION, positionLight, 0);

        // Normalize normals
        gl.glEnable(GL_NORMALIZE);
    }

    /**
     * Configures the viewing transform.
     */
    @Override
    public void setView() {
        // Select part of window.
        gl.glViewport(0, 0, gs.w, gs.h);

        // Set projection matrix.
        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadIdentity();

        // Set the perspective to match the field of view of the camera.
        double aspectRatio = (double) gs.w / (double) gs.h;
        double fieldOfView = 2.0 * toDegrees(atan2(gs.vWidth / aspectRatio, 2 * gs.vDist));
        glu.gluPerspective(fieldOfView, (float) aspectRatio, 0.1 * gs.vDist, 10 * gs.vDist);

        // Set camera.
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();

        // Update the view according to the camera mode and robot of interest.
        // For camera modes 1 to 4, determine which robot to focus on.
        camera.update(gs, robots[2]);
        glu.gluLookAt(camera.eye.x(), camera.eye.y(), camera.eye.z(),
                camera.center.x(), camera.center.y(), camera.center.z(),
                camera.up.x(), camera.up.y(), camera.up.z());

        // Set position light source #0 to an infinite source, coming from the camera. (Does not really make a difference though)
        //float sunlightPosition[] = {(float) camera.eye.x(), (float) camera.eye.y(), (float) camera.eye.z(), 1.0f};
        //gl.glLightfv(GL_LIGHT0, GL_POSITION, sunlightPosition, 0);
    }

    /**
     * Draws the entire scene.
     */
    @Override
    public void drawScene() {
        // Background color.
        gl.glClearColor(1f, 1f, 1f, 0f);

        // Clear background.
        gl.glClear(GL_COLOR_BUFFER_BIT);

        // Clear depth buffer.
        gl.glClear(GL_DEPTH_BUFFER_BIT);

        // Set color to black.
        gl.glColor3f(0f, 0f, 0f);

        gl.glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        // Draw the axis frame.
        if (gs.showAxes) {
            new AxisFrame().draw(gl, glu, glut);
        }

        // Draw all robots.
        for (int i = 0; i < robots.length; i++) {
            robots[i].walk(); // Determine a place on the track where the robot should stand
            robots[i].setPosition(raceTracks[gs.trackNr].getLanePoint(i, robots[i].getPositionOnTrack()));  // Update the robo's position
            robots[i].setDirection(
                    raceTracks[gs.trackNr].getLaneTangent(i, robots[i].getPositionOnTrack()),
                    raceTracks[gs.trackNr].getLaneTangent(i, 0)); // Update the robo's direction
            robots[i].draw(gl, glu, glut, gs.showStick, gs.tAnim);
        }

        // Draw the race track.
        raceTracks[gs.trackNr].draw(gl, glu, glut);

        // Draw the terrain.
        terrain.draw(gl, glu, glut);
    }

    /**
     * Main program execution body, delegates to an instance of the RobotRace
     * implementation.
     *
     * @param args
     */
    public static void main(String args[]) {
        RobotRace robotRace = new RobotRace();
        robotRace.run();
    }
}
