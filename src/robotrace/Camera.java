package robotrace;

import robotrace.robot.Robot;
import static java.lang.Math.*;

/**
 * Implementation of a camera with a position and orientation.
 */
class Camera {

    // The position of the camera.
    public Vector eye = new Vector(3f, 6f, 5f);

    // The point to which the camera is looking.
    public Vector center = Vector.O;

    // The up vector.
    public Vector up = Vector.Z;

    // The amount of seconds to wait until switching to the next camera
    private static final int AUTO_CAMERA_SWITCH_FREQUENCY = 5;

    /**
     * Updates the camera viewpoint and direction based on the selected camera
     * mode.
     *
     */
    public void update(GlobalState gs, Robot focus) {
        switch (gs.camMode) {

            // Helicopter mode
            case 1:
                setHelicopterMode(gs, focus);
                break;

            // Motor cycle mode    
            case 2:
                setMotorCycleMode(gs, focus);
                break;

            // First person mode    
            case 3:
                setFirstPersonMode(gs, focus);
                break;

            // Auto mode    
            case 4:
                setAutoMode(gs, focus);
                break;

            // Default mode    
            default:
                setDefaultMode(gs);
        }
    }

    /**
     * Computes eye, center, and up, based on the camera's default mode.
     */
    private void setDefaultMode(GlobalState gs) {
        // Convert angles to relative angles
        double thetaRelative = -gs.theta;
        double phiRelative = PI / 2 - gs.phi;

        // Vector between center point and eye point.
        Vector v = new Vector(
                gs.vDist * cos(thetaRelative) * sin(phiRelative),
                gs.vDist * sin(thetaRelative) * sin(phiRelative),
                gs.vDist * cos(phiRelative))
                .add(gs.cnt);

        // Eyepoint e is center point + vector
        this.eye = new Vector(
                gs.cnt.x + v.x,
                gs.cnt.y + v.y,
                gs.cnt.z + v.z);

        // Set camera centerpoint to centerpoint
        this.center = gs.cnt;
        this.up = Vector.Z;
    }

    /**
     * Computes eye, center, and up, based on the helicopter mode. The camera
     * should focus on the robot.
     */
    private void setHelicopterMode(GlobalState gs, Robot focus) {
        // The center point is the position of the robot.
        this.center = focus.getPosition();

        // The eyepoint is center point an offset to the sky.
        this.eye = this.center.add(new Vector(0, 0, 20));

        // Set the up-vector as the direction of the selected robot.
        this.up = focus.getDirection();
    }

    /**
     * Computes eye, center, and up, based on the motorcycle mode. The camera
     * should focus on the robot.
     */
    private void setMotorCycleMode(GlobalState gs, Robot focus) {
        // The eyepoint is the robot position with a lttle offset to the side.
        Vector offset = new Vector(5, 0, -2);
        final Vector focusVec = focus.getDirection().normalized();
        this.eye = focusVec.cross(new Vector(-focusVec.x, -focusVec.y, focusVec.z + 1)).subtract(offset);

        // The center point is the position of the robot that is being followed, but with a little offset.
        this.center = focus.getPosition().add(new Vector(0, 0, 2));

        // The up vector is the Z unit vector.
        this.up = Vector.Z;
    }

    /**
     * Computes eye, center, and up, based on the first person mode. The camera
     * should view from the perspective of the robot.
     */
    private void setFirstPersonMode(GlobalState gs, Robot focus) {
        // the camera should be positioned at the position of the robot, as you are looking from its position.
        this.eye = focus.getPosition().add(new Vector(0, 0, 2));

        // The robot has to look in the direction it is walking in, from the position it is at!
        // Determine vector v, which is the difference in position of the camera, and the centerpoint.
        Vector v = focus.getDirection();
        v.z = 0; // Force z vector to be 0! Yes the robot could be climbing up, having a Z direction but we omit this
        this.center = focus.getPosition().subtract(v);

        // The up vector is the Z unit vector.
        this.up = Vector.Z;
    }

    /**
     * Computes eye, center, and up, based on the auto mode. The above modes are
     * alternated.
     */
    private void setAutoMode(GlobalState gs, Robot focus) {
        switch (((int) gs.tAnim) / AUTO_CAMERA_SWITCH_FREQUENCY % 4) {
            // Default mode
            case 0:
                setDefaultMode(gs);
                break;

            // Helicopter mode
            case 1:
                setHelicopterMode(gs, focus);
                break;

            // Motor cycle mode    
            case 2:
                setMotorCycleMode(gs, focus);
                break;

            // First person mode    
            case 3:
                setFirstPersonMode(gs, focus);
                break;
        }
    }
}
