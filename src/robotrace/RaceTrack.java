package robotrace;

import com.jogamp.opengl.util.gl2.GLUT;
import static java.lang.Math.*;
import static javax.media.opengl.GL.GL_REPEAT;
import static javax.media.opengl.GL.GL_TEXTURE_2D;
import static javax.media.opengl.GL.GL_TEXTURE_WRAP_S;
import static javax.media.opengl.GL.GL_TEXTURE_WRAP_T;
import javax.media.opengl.GL2;
import static javax.media.opengl.GL2.GL_QUAD_STRIP;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_COLOR_MATERIAL;
import javax.media.opengl.glu.GLU;
import static robotrace.Base.brick;
import static robotrace.Base.track;

/**
 * Implementation of a race track that is made from Bezier segments.
 */
class RaceTrack {

    // The width of one lane. The total width of the track is 4 * laneWidth.
    private final static float LANE_WIDTH = 1.22f;

    // Array with 3N control points, where N is the number of segments.
    private Vector[] controlPoints = null;

    /**
     * Constructor for the default race track.
     */
    public RaceTrack() {
    }

    /**
     * Constructor for a spline race track.
     */
    public RaceTrack(Vector[] controlPoints) {
        this.controlPoints = controlPoints;
    }

    /**
     * Draws the default race track, without use of bezier splines. Used when
     * control points are unavailable
     *
     * @param numEdges amount of edges to use to draw the track
     * @param numLanes amount of lanes to generate, generally equal to the
     * amount of robots
     * @param height height of the track
     */
    private void drawRaceTrack(GL2 gl, final int numEdges, final int numLanes, final float height) {
        float position = 1.0f / numEdges;
        float widthTexture = brick.getWidth();

        // Enable the brick texture. We use it to texture the sides of the track.
        if (brick != null) {
            gl.glColor3f(1f, 1f, 1f);
            brick.enable(gl);
            brick.bind(gl);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            gl.glEnable(GL_COLOR_MATERIAL);
        }

        // Draw sides of the lanes
        Material.applyMaterialToGL(gl, Material.VOID);
        for (int j = 0; j <= numLanes; j += numLanes) {
            gl.glBegin(GL_QUAD_STRIP);
            for (int i = 0; i <= numEdges; ++i) {
                // Get the next point on the shape boundary.
                float currentPosition = position * i;
                Vector point = getPoint(currentPosition);

                // Get tangent vector.
                Vector tangent = getTangent(currentPosition);

                // Calculate the normal vector from the tangent vector.
                Vector normal = new Vector(tangent.y, -tangent.x, 0);

                // Determine the unit length
                float length = (float) sqrt((normal.x * normal.x) + (normal.y * normal.y));

                // Set the point for the top part of the track
                Vector p1 = new Vector(
                        point.x + normal.x / length * LANE_WIDTH * j, // Normalize normal vector and rescale normal vector.
                        point.y + normal.y / length * LANE_WIDTH * j, // Normalize normal vector and rescale normal vector.
                        point.z);

                // Set the point for the bottom part of the track
                Vector p2 = new Vector(
                        point.x + normal.x / length * LANE_WIDTH * j, // Normalize normal vector and rescale normal vector.
                        point.y + normal.y / length * LANE_WIDTH * j, // Normalize normal vector and rescale normal vector.
                        point.z - height);  // Reduce the Z axis by the height of the race track

                // Determine the normal of the previous quad
                if (j == 0) {
                    gl.glNormal3d(-normal.x, -normal.y, -normal.z);
                } else {
                    gl.glNormal3d(normal.x, normal.y, normal.z);
                }

                // Add top and bottom points to draw, including texture coordinates.
                final float texCoord = currentPosition * widthTexture;
                gl.glTexCoord2d(texCoord, p1.z);
                gl.glVertex3d(p1.x, p1.y, p1.z);
                gl.glTexCoord2d(texCoord, p2.z);
                gl.glVertex3d(p2.x, p2.y, p2.z);
            }
            gl.glEnd();
        }

        // Disable the brick texture, we now finished drawing the sides of the track.
        if (brick != null) {
            brick.disable(gl);
            gl.glDisable(GL_COLOR_MATERIAL);
        }

        // Old values of the points
        Vector _p1 = null;
        Vector _p2 = null;

        // Enable track texture, we now want to draw the top of the track.
        if (track != null) {
            track.enable(gl);
            track.bind(gl);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            gl.glEnable(GL_COLOR_MATERIAL);
        }

        widthTexture = track.getWidth();

        // Draw top of the lanes
        for (int j = 0; j <= numLanes - 1; ++j) {
            gl.glBegin(GL_QUAD_STRIP);
            for (int i = 0; i <= numEdges; ++i) {
                // Get the next point on the shape boundary.
                float currentPosition = position * i;
                Vector point = getPoint(currentPosition);

                // Get tangent vector.
                Vector tangent = getTangent(currentPosition);

                // Calculate the normal vector from the tangent vector.
                Vector normal = new Vector(tangent.y, -tangent.x, 0);

                // Determine the unit length
                float length = (float) sqrt((normal.x * normal.x) + (normal.y * normal.y));

                // Set the point for the current lane of the track
                Vector p1 = new Vector(
                        point.x + normal.x / length * LANE_WIDTH * j, // Normalize normal vector and rescale normal vector.
                        point.y + normal.y / length * LANE_WIDTH * j, // Normalize normal vector and rescale normal vector.
                        point.z);

                // Set the point for the next lane of the track
                Vector p2 = new Vector(
                        point.x + normal.x / length * LANE_WIDTH * (j + 1), // Normalize normal vector and rescale normal vector as if it were on the next lane.
                        point.y + normal.y / length * LANE_WIDTH * (j + 1), // Normalize normal vector and rescale normal vector as if it were on the next lane.
                        point.z);

                // Determine the normals of the quad, using previous and current points
                if (_p1 != null || _p2 != null) {
                    Vector edgeHorizontal = p1.subtract(_p1);
                    Vector edgeVertical = p2.subtract(_p1);
                    Vector quadNormal = edgeVertical.cross(edgeHorizontal);
                    gl.glNormal3d(quadNormal.x, quadNormal.y, quadNormal.z);
                }

                // Add top and bottom points to draw
                final float texCoord = currentPosition * widthTexture;
                gl.glTexCoord2d(texCoord, p1.x);
                gl.glVertex3d(p1.x, p1.y, p1.z);
                gl.glTexCoord2d(texCoord, p2.x);
                gl.glVertex3d(p2.x, p2.y, p2.z);

                // Set current points as old points, which will be used to determine the normal, next iteration
                _p1 = p1;
                _p2 = p2;
            }
            gl.glEnd();
        }

        // Disable the track texture, we finished drawing the top of the track.
        if (track != null) {
            track.disable(gl);
            gl.glDisable(GL_COLOR_MATERIAL);
        }
    }

    /**
     * Draws a bezier spline based track. Used when control points are
     * available.
     *
     * @param numLanes amount of lanes to generate, generally equal to the
     * amount of robots
     * @param height height of the track
     */
    private void drawSplineTrack(GL2 gl, final int numLanes, final float height) {
        final int numEdges = this.controlPoints.length;
        final float position = 1.0f / (numEdges * 4);
        float widthTexture = brick.getWidth();

        // Enable the brick texture. We use it to texture the sides of the track.
        if (brick != null) {
            gl.glColor3f(1f, 1f, 1f);
            brick.enable(gl);
            brick.bind(gl);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            gl.glEnable(GL_COLOR_MATERIAL);
        }

        // Draw sides of spline track.
        Material.applyMaterialToGL(gl, Material.VOID);
        for (int j = 0; j <= numLanes; j += numLanes) {
            gl.glBegin(GL_QUAD_STRIP);
            for (int i = 0; i <= (numEdges * 4); ++i) {
                final float currentPosition = i * position;
                final Vector point = this.getCubicBezierPoint(currentPosition);
                final Vector tangent = this.getCubicBezierTangent(currentPosition);
                final Vector normal = new Vector(tangent.y(), -tangent.x(), 0);
                final float length = (float) Math.sqrt((normal.x() * normal.x()) + (normal.y() * normal.y()));
                final Vector p1 = new Vector(point.x() + normal.x() / length * LANE_WIDTH * j, point.y() + normal.y() / length * LANE_WIDTH * j, point.z());
                final Vector p2 = new Vector(point.x() + normal.x() / length * LANE_WIDTH * j, point.y() + normal.y() / length * LANE_WIDTH * j, point.z() - height);
                
                // Calculate texture coordinates for the track.
                final float texCoord = currentPosition * widthTexture;
                gl.glTexCoord2d(texCoord, p1.z);
                gl.glVertex3d(p1.x(), p1.y(), p1.z());
                gl.glTexCoord2d(texCoord, p2.z);
                gl.glVertex3d(p2.x(), p2.y(), p2.z());

                // Determine the normal of the previous quad
                if (j == 0) {
                    gl.glNormal3d(-normal.x, -normal.y, -normal.z);
                } else {
                    gl.glNormal3d(normal.x, normal.y, normal.z);
                }
            }
            gl.glEnd();
        }

        // Disable the brick texture, we now finished drawing the sides of the track.
        if (brick != null) {
            brick.disable(gl);
            gl.glDisable(GL_COLOR_MATERIAL);
        }

        // Old values of the points
        Vector _p1 = null;
        Vector _p2 = null;

        // Enable track texture, we now want to draw the top of the track.
        if (track != null) {
            track.enable(gl);
            track.bind(gl);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            gl.glEnable(GL_COLOR_MATERIAL);
        }

        widthTexture = track.getWidth();

        // Draw top of spline track.
        for (int j = 0; j <= numLanes - 1; ++j) {
            gl.glBegin(GL_QUAD_STRIP);
            for (int i = 0; i <= (numEdges * 4); ++i) {
                final float currentPosition = i * position;
                final Vector point = this.getCubicBezierPoint(currentPosition);
                final Vector tangent = this.getCubicBezierTangent(currentPosition);
                final Vector normal = new Vector(tangent.y(), -tangent.x(), 0);
                final float length = (float) Math.sqrt((normal.x() * normal.x()) + (normal.y() * normal.y()));

                // Set the point for the current lane of the track
                Vector p1 = new Vector(
                        point.x + normal.x / length * LANE_WIDTH * j, // Normalize normal vector and rescale normal vector.
                        point.y + normal.y / length * LANE_WIDTH * j, // Normalize normal vector and rescale normal vector.
                        point.z);

                // Set the point for the next lane of the track
                Vector p2 = new Vector(
                        point.x + normal.x / length * LANE_WIDTH * (j + 1), // Normalize normal vector and rescale normal vector as if it were on the next lane.
                        point.y + normal.y / length * LANE_WIDTH * (j + 1), // Normalize normal vector and rescale normal vector as if it were on the next lane.
                        point.z);

                // Determine the normals of the quad, using previous and current points
                if (_p1 != null || _p2 != null) {
                    Vector edgeHorizontal = p1.subtract(_p1);
                    Vector edgeVertical = p2.subtract(_p1);
                    Vector quadNormal = edgeVertical.cross(edgeHorizontal);
                    gl.glNormal3d(quadNormal.x, quadNormal.y, quadNormal.z);
                }

                // Calculate texture coordinates for the track.
                final float texCoord = currentPosition * widthTexture;
                gl.glTexCoord2d(texCoord, p1.x);
                gl.glVertex3d(p1.x(), p1.y(), p1.z());
                gl.glTexCoord2d(texCoord, p2.x);
                gl.glVertex3d(p2.x(), p2.y(), p2.z());

                // Set current points as old points, which will be used to determine the normal, next iteration
                _p1 = p1;
                _p2 = p2;
            }
            gl.glEnd();
        }

        // Disable the track texture, we finished drawing the top of the track.
        if (track != null) {
            track.disable(gl);
            gl.glDisable(GL_COLOR_MATERIAL);
        }

    }

    /**
     * Draws a race track.
     */
    public void draw(GL2 gl, GLU glu, GLUT glut) {
        if (controlPoints == null) {
            this.drawRaceTrack(gl, 64, 4, 2);
        } else {
            this.drawSplineTrack(gl, 4, 2);
        }
    }

    /**
     * Returns the center of a lane at 0 <= t < 1. Use this method to find the
     * position of a robot on the track.
     */
    public Vector getLanePoint(int lane, double t) {
        if (controlPoints == null) {
            Vector point = getPoint(t);

            // Get tangent vector.
            Vector tangent = getTangent(t);

            // Calculate the normal vector from the tangent vector.
            Vector normal = new Vector(tangent.y, -tangent.x, 0);

            // Normalize normal vector.
            float length = (float) sqrt((normal.x * normal.x) + (normal.y * normal.y));
            if (length != 0.0f) {
                length = 1.0f / length;
            }
            normal.x *= length;
            normal.y *= length;

            // Rescale normal vector.
            normal.x *= LANE_WIDTH * (lane) + LANE_WIDTH / 2;
            normal.y *= LANE_WIDTH * (lane) + LANE_WIDTH / 2;

            return new Vector(point.x + normal.x, point.y + normal.y, point.z);
        } else {
            Vector v = this.getCubicBezierPoint(t);
            final Vector tangent = this.getCubicBezierTangent(t);
            final Vector normal = new Vector(tangent.y(), -tangent.x(), 0);
            float length = (float) Math.sqrt((normal.x() * normal.x()) + (normal.y() * normal.y()));
            if (length != 0.0f) {
                length = 1.0f / length;
            }
            normal.x *= length;
            normal.y *= length;

            // Rescale normal vector.
            normal.x *= LANE_WIDTH * (lane) + LANE_WIDTH / 2;
            normal.y *= LANE_WIDTH * (lane) + LANE_WIDTH / 2;

            return new Vector(v.x + normal.x, v.y + normal.y, v.z);
        }
    }

    /**
     * Returns the tangent of a lane at 0 <= t < 1. Use this method to find the
     * orientation of a robot on the track.
     */
    public Vector getLaneTangent(int lane, double t) {
        if (controlPoints == null) {
            // Get tangent vector.
            Vector tangent = getTangent(t);

            // Calculate the normal vector from the tangent vector.
            Vector normal = new Vector(tangent.y, -tangent.x, 0);

            // Normalize normal vector.
            float length = (float) sqrt((normal.x * normal.x) + (normal.y * normal.y));
            if (length != 0.0f) {
                length = 1.0f / length;
            }
            normal.x *= length;
            normal.y *= length;

            // Rescale normal vector.
            normal.x *= LANE_WIDTH * (lane) + LANE_WIDTH / 2;
            normal.y *= LANE_WIDTH * (lane) + LANE_WIDTH / 2;
            
            // Adding the normal vector to the tangent is not a good idea, so we did it
            // differently. We calculate the cross product of the normal vector and its
            // clone pointing upwards. This way we get a vector in the direction of the tangent.
            return normal.cross(new Vector(normal.x, normal.y, normal.z + 1));
            //return new Vector(tangent.x + normal.x, tangent.y + normal.y, tangent.z);
        } else {
            Vector v = this.getCubicBezierTangent(t);
            final Vector normal = new Vector(v.y(), -v.x(), 0);
            float length = (float) Math.sqrt((normal.x() * normal.x()) + (normal.y() * normal.y()));
            if (length != 0.0f) {
                length = 1.0f / length;
            }
            normal.x *= length;
            normal.y *= length;

            // Rescale normal vector.
            normal.x *= LANE_WIDTH * (lane) + LANE_WIDTH / 2;
            normal.y *= LANE_WIDTH * (lane) + LANE_WIDTH / 2;

            // Adding the normal vector to the tangent is not a good idea, so we did it
            // differently. We calculate the cross product of the normal vector and its
            // clone pointing upwards. This way we get a vector in the direction of the tangent.
            return normal.cross(new Vector(normal.x, normal.y, normal.z + 1));
            //return new Vector(v.x + normal.x, v.y + normal.y, v.z);
        }
    }

    /**
     * Returns a point on the test track at 0 <= t < 1.
     */
    private Vector getPoint(double t) {
        return new Vector(10 * cos(2 * PI * t), 14 * sin(2 * PI * t), 1);
    }

    /**
     * Returns a tangent on the test track at 0 <= t < 1.
     */
    private Vector getTangent(double t) {
        // We derive "P(t) = (10 cos(2πt), 14 sin(2πt), 1)"
        return new Vector(-20 * sin(2 * PI * t) * PI, 28 * cos(2 * PI * t) * PI, 0);
    }

    private final Vector getBezierFromControlPoins(final float t, Vector P0, Vector P1, Vector P2, Vector P3) {
        // Calculate point on Bezier curve using cubic Bezier. (1 + 3 + 3 + 1)
        final float at = 1 - t;
        final float cp0 = at * at * at;
        final float cp1 = 3 * at * at * t;
        final float cp2 = 3 * at * t * t;
        final float cp3 = t * t * t;

        // Scale the output vectors with the calculated points.
        Vector outvec0 = P0.scale(cp0);
        Vector outvec1 = P1.scale(cp1);
        Vector outvec2 = P2.scale(cp2);
        Vector outvec3 = P3.scale(cp3);

        // Add calculation to output vector.
        Vector retVal = new Vector(0, 0, 1);
        return retVal.add(outvec0.add(outvec1).add(outvec2).add(outvec3));
    }

    /**
     * Returns a point on a bezier segment with control points P0, P1, P2, P3 at
     * 0 <= t < 1.
     */
    private Vector getCubicBezierPoint(double t/*, Vector P0, Vector P1, Vector P2, Vector P3*/) {
        // The curve consists of sections of control points. Find out in which segment the parameter 'f' is.
        final int amountOfSections = (this.controlPoints.length - 1) / 3;
        t %= 1;
        final int section = (int) (t * amountOfSections);
        int offset = section * 3;
        final float mapped_t = (float) (t * amountOfSections) % 1;

        // Calculate the Bezier curve from the four control points.
        return this.getBezierFromControlPoins(mapped_t, this.controlPoints[offset++], this.controlPoints[offset++], this.controlPoints[offset++], this.controlPoints[offset]);
    }

    private Vector getQuadraticBezier(final float t, Vector P0, Vector P1, Vector P2) {
        // Calculate point on curve using quadratic Bezier. (1 + 2 + 1)
        final float at = 1 - t;
        final float cp0 = at * at;      // (1 - t) ^ 2
        final float cp1 = 2 * at * t;   // 2 * (1 - t) * t
        final float cp2 = t * t;        // t ^ 2

        // Scale the output vertices to add weight to each of the points.
        Vector outvec0 = P0.scale(cp0);
        Vector outvec1 = P1.scale(cp1);
        Vector outvec2 = P2.scale(cp2);

        // Add calculations to output vector.
        return outvec0.add(outvec1).add(outvec2);
    }

    private Vector getBezierTangentFromControlPoins(final float t, Vector P0, Vector P1, Vector P2, Vector P3) {
        // Subtract vectors from each other to create quadratic parameters.
        Vector outvec0 = P1.subtract(P0);
        Vector outvec1 = P2.subtract(P1);
        Vector outvec2 = P3.subtract(P2);

        // Get the quadratic Bezier curve. This is the tangent of the cubic one.
        return this.getQuadraticBezier(t, outvec0, outvec1, outvec2).scale(3);
    }

    /**
     * Returns a tangent on a bezier segment with control points P0, P1, P2, P3
     * at 0 <= t < 1.
     */
    private Vector getCubicBezierTangent(double t/*, Vector P0, Vector P1, Vector P2, Vector P3*/) {
        // The curve consists of sections of control points. Find out in which segment the parameter 'f' is.
        final int amountOfSections = (this.controlPoints.length - 1) / 3;
        t %= 1;
        final int section = (int) (t * amountOfSections);
        int offset = section * 3;
        final float mapped_t = (float) (t * amountOfSections) % 1;

        // Calculate the Bezier curve from the four control points.
        return this.getBezierTangentFromControlPoins(mapped_t, this.controlPoints[offset++], this.controlPoints[offset++], this.controlPoints[offset++], this.controlPoints[offset]);
    }
}
