package robotrace;

import static javax.media.opengl.GL.GL_FRONT_AND_BACK;
import static javax.media.opengl.GL2.*;
import javax.media.opengl.GL2;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_AMBIENT;

/**
 * Materials that can be used for the robots or other world objects.
 */
public enum Material {

    GOLD(
            new float[]{1.0f, 0.8f, 0.1f, 1.0f},
            new float[]{0.8f, 0.6f, 0.2f, 1.0f},
            50f),
    SILVER(
            new float[]{0.8f, 0.8f, 0.8f, 1.0f},
            new float[]{0.6f, 0.6f, 0.6f, 1.0f},
            75f),
    WOOD(
            new float[]{0.6f, 0.4f, 0.1f, 1.0f},
            new float[]{0.01f, 0.01f, 0.01f, 1.0f},
            25f),
    ORANGE_PLASTIC(
            new float[]{1.0f, 0.5f, 0.0f, 1.0f},
            new float[]{0.02f, 0.01f, 0.0f, 1.0f},
            25f),
    WATER(
            new float[]{0.0f, 0.5f, 0.8f, 1.0f},
            new float[]{0.2f, 0.2f, 0.2f, 1.0f},
            100f),
    SAND(
            new float[]{0.8f, 0.8f, 0.2f, 1.0f},
            new float[]{0.0f, 0.0f, 0.0f, 1.0f},
            25f),
    GRASS(
            new float[]{0.2f, 1.0f, 0.2f, 1.0f},
            new float[]{0.0f, 0.2f, 0.2f, 1.0f},
            25f),
    TRANSPARENT(
            new float[]{0.0f, 0.3f, 0.9f, 0.2f},
            new float[]{0.0f, 0.3f, 0.9f, 0.2f},
            100f),
    VOID(
            new float[]{0.0f, 0.0f, 0.0f, 1.0f},
            new float[]{0.0f, 0.0f, 0.0f, 1.0f},
            0f),
    WHITE(
            new float[]{1.0f, 1.0f, 1.0f, 1.0f},
            new float[]{1.0f, 1.0f, 1.0f, 1.0f},
            0f);

    /**
     * The diffuse RGBA reflectance of the material.
     */
    private float[] diffuse;

    /**
     * The specular RGBA reflectance of the material.
     */
    private float[] specular;

    /**
     * The specular exponent of the material.
     */
    private float shininess;

    /**
     * Constructs a new material with diffuse and specular properties.
     */
    private Material(float[] diffuse, float[] specular, float shininess) {
        this.diffuse = diffuse;
        this.specular = specular;
        this.shininess = shininess;
    }

    public float[] getDiffuse() {
        return diffuse;
    }

    public void setDiffuse(float[] diffuse) {
        this.diffuse = diffuse;
    }

    public float[] getSpecular() {
        return specular;
    }

    public void setSpecular(float[] specular) {
        this.specular = specular;
    }

    public float getShininess() {
        return shininess;
    }

    public void setShininess(float shininess) {
        this.shininess = shininess;
    }

    /**
     * Applies the attributes of the given material to openGL
     *
     * @param gl
     * @param material
     */
    public static void applyMaterialToGL(GL2 gl, Material material) {
        gl.glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material.getDiffuse(), 0);
        gl.glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material.getSpecular(), 0);
        gl.glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, material.getShininess());
        gl.glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, new float[]{0.0f, 0.0f, 0.0f, 1.0f}, 0);
    }
}
