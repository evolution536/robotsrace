package robotrace;

public class BezierHelper {

    public BezierHelper() {

    }

    /**
     * Generates the O-shaped spline track.
     *
     * @param radius1 Horizontal radius of the circle
     * @param radius2 Vertical radius of the circle
     * @param numEdges Amount of edges in the pseudo circle
     * @param elevation Moves the first half of the track to the amount, then
     * decreases it back to 0
     * @return Returns the control points of the bezier
     */
    public Vector[] generateEllipsizedTrack(final float radius1, final float radius2, final int numEdges, int elevation) {
        Vector[] outvecs = new Vector[numEdges];
        final float edgeSize = 1.0f / numEdges;
        int i = 0;
        for (float t = 0.0f; t < 1.0f; t += edgeSize) {
            final float px = radius1 * (float) Math.cos(2 * Math.PI * t);
            final float py = radius2 * (float) Math.sin(2 * Math.PI * t);

            if (t < 0.5) {
                outvecs[i++] = new Vector(px, py, t * elevation);
            } else {
                outvecs[i++] = new Vector(px, py, (0.5f * elevation) + ((0.5f - t) * elevation));
            }
        }
        return outvecs;
    }

    /**
     * Generates the L-shaped spline track.
     *
     * @return Returns the control points of the bezier
     */
    public Vector[] generateCapitalLtrack() {
        final double r = 2;
        final double h = 20;
        final double w = 15;
        final double h1 = h - 2 * r;
        final double h2 = h1 - 2 * r;
        final double w1 = w - 2 * r;
        final double w2 = w1 - 2 * r;
        final double cp = r * 4 / 3;
        final double offset_x = -1.5;
        final double offset_y = 10;

        final Vector[] controlPoints = new Vector[25];
        controlPoints[0] = new Vector(offset_x, 10, 0);
        controlPoints[1] = new Vector(offset_x, 10 + cp, 0);
        controlPoints[2] = new Vector(-2 * r + offset_x, offset_y + cp, 0);
        controlPoints[3] = new Vector(-2 * r + offset_x, offset_y, 0);
        controlPoints[4] = new Vector(-2 * r + offset_x, offset_y + -h1 / 3, 0);
        controlPoints[5] = new Vector(-2 * r + offset_x, offset_y + -2 * h1 / 3, 0);
        controlPoints[6] = new Vector(-2 * r + offset_x, offset_y + -h1, 0);
        controlPoints[7] = new Vector(-2 * r + offset_x, offset_y + -h1 - 0.5 * r, 0);
        controlPoints[8] = new Vector(-1.5 * r + offset_x, offset_y + -h1 - r, 0);
        controlPoints[9] = new Vector(-r + offset_x, offset_y + -h1 - r, 0);
        controlPoints[10] = new Vector(-r + offset_x + w1 / 3, offset_y + -h1 - r, 0);
        controlPoints[11] = new Vector(-r + 2 * w1 / 3 + offset_x, offset_y + -h1 - r, 0);
        controlPoints[12] = new Vector(-r + offset_x + w1, offset_y + -h1 - r, 0);
        controlPoints[13] = new Vector(-r + offset_x + w1 + cp, offset_y + -h1 - r, 0);
        controlPoints[14] = new Vector(-r + offset_x + w1 + cp, offset_y + -h1 + r, 0);
        controlPoints[15] = new Vector(-r + offset_x + w1, offset_y + -h1 + r, 0);
        controlPoints[16] = new Vector(r + 2 * w2 / 3 + offset_x, offset_y + -h1 + r, 0);
        controlPoints[17] = new Vector(r + offset_x + w2 / 3, offset_y + -h1 + r, 0);
        controlPoints[18] = new Vector(r + offset_x, offset_y + -h1 + r, 0);
        controlPoints[19] = new Vector(0.5 * r + offset_x, offset_y + -h1 + r, 0);
        controlPoints[20] = new Vector(offset_x, offset_y + -h1 + 1.5 * r, 0);
        controlPoints[21] = new Vector(offset_x, offset_y + -h1 + 2 * r, 0);
        controlPoints[22] = new Vector(offset_x, offset_y + -2 * h2 / 3, 0);
        controlPoints[23] = new Vector(offset_x, offset_y + -h2 / 3, 0);
        controlPoints[24] = new Vector(offset_x, offset_y, 0);
        return controlPoints;
    }

    /**
     * Generates the C-shaped spline track.
     *
     * @return Returns the control points of the bezier
     */
    public Vector[] generateCapitalCTrack() {
        final double radius = 12;
        final double cp = radius * 4 / 3;
        final double tp = 2.5;
        final double cptp = tp * 4 / 3;
        final double offset_x = radius / 2 - 4;
        final double offset_y = -radius;
        double inner_radius = radius - 2 * tp;
        double cpir = inner_radius * 4 / 3;

        final Vector[] controlPoints = new Vector[13];
        controlPoints[0] = new Vector(offset_x, offset_y, 0);
        controlPoints[1] = new Vector(cptp + offset_x, offset_y, 0);
        controlPoints[2] = new Vector(cptp + offset_x, 2 * tp + offset_y, 0);
        controlPoints[3] = new Vector(offset_x, 2 * tp + offset_y, 0);
        controlPoints[4] = new Vector(-cpir + offset_x, 2 * tp + offset_y, 0);
        controlPoints[5] = new Vector(-cpir + offset_x, 2 * radius - 2 * tp + offset_y, 0);
        controlPoints[6] = new Vector(offset_x, 2 * radius - 2 * tp + offset_y, 0);
        controlPoints[7] = new Vector(cptp + offset_x, 2 * radius - 2 * tp + offset_y, 0);
        controlPoints[8] = new Vector(cptp + offset_x, 2 * radius + offset_y, 0);
        controlPoints[9] = new Vector(offset_x, 2 * radius + offset_y, 0);
        controlPoints[10] = new Vector(-cp + offset_x, 2 * radius + offset_y, 0);
        controlPoints[11] = new Vector(-cp + offset_x, +offset_y, 0);
        controlPoints[12] = new Vector(offset_x, +offset_y, 0);
        return controlPoints;
    }
}
