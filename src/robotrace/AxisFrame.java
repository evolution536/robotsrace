package robotrace;

import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_LIGHTING;
import javax.media.opengl.glu.GLU;

public class AxisFrame {

    public AxisFrame() {

    }

    /**
     * Draws an arrow (scaled cube with cone on top) in the specified
     * orientation with the specified color.
     *
     * @param orientation The orientation of the arrow. 0: x, 1: y, 2: z.
     * @param r The red color value.
     * @param g The green color value.
     * @param b The blue color value.
     */
    private void drawArrow(GL2 gl, GLU glu, GLUT glut,
            int orientation, float r, float g, float b) {
        // Set the color to the specified to continue drawing using the customized color.
        gl.glPushMatrix();
        gl.glColor3f(r, g, b);

        // Make a distinction using the orientation that was specified.
        switch (orientation) {
            case 0:
                gl.glScalef(1.0f, 0.05f, 0.05f);
                glut.glutSolidCube(1.0f);
                gl.glScalef(1.0f, 20.0f, 20.0f);
                gl.glRotatef(270.0f, 0.0f, 1.0f, 0.0f);
                gl.glTranslatef(0.0f, 0.0f, 0.5f);
                glut.glutSolidCone(0.2, 0.5, 25, 20);
                break;
            case 1:
                gl.glScalef(0.05f, 1.0f, 0.05f);
                glut.glutSolidCube(1.0f);
                gl.glScalef(20.0f, 1.0f, 20.0f);
                gl.glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
                gl.glTranslatef(0.0f, 0.0f, 0.5f);
                glut.glutSolidCone(0.2, 0.5, 25, 20);
                break;
            case 2:
                gl.glScalef(0.05f, 0.05f, 1.0f);
                glut.glutSolidCube(1.0f);
                gl.glScalef(20.0f, 20.0f, 1.0f);
                gl.glRotatef(270.0f, 0.0f, 0.0f, 1.0f);
                gl.glTranslatef(0.0f, 0.0f, 0.5f);
                glut.glutSolidCone(0.2, 0.5, 25, 20);
                break;
        }

        gl.glPopMatrix();
    }

    /**
     * Draws the x-axis (red), y-axis (green), z-axis (blue), and origin
     * (yellow).
     */
    public void draw(GL2 gl, GLU glu, GLUT glut) {
        gl.glPushMatrix();

        // We chose to disable GL_LIGHTING before drawing axis' and to enable them afterwards.
        // Reason: We think that the axes are not real objects in our Robot world, just something for the developers.
        gl.glDisable(GL_LIGHTING);

        // Draw three axis' in red, green and blue.
        drawArrow(gl, glu, glut, 0, 1.0f, 0.0f, 0.0f);
        drawArrow(gl, glu, glut, 1, 0.0f, 1.0f, 0.0f);
        drawArrow(gl, glu, glut, 2, 0.0f, 0.0f, 1.0f);

        // Draw origin as sphere in yellow, by setting scale and colors first.
        gl.glColor3f(1.0f, 1.0f, 0.0f);
        glut.glutSolidSphere(0.15, 25, 20);

        // Reset colors used for axis cubes.
        gl.glColor3f(0.0f, 0.0f, 0.0f);
        gl.glEnable(GL_LIGHTING);

        gl.glPopMatrix();
    }

}
