package robotrace.terrain;

import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import robotrace.Material;
import robotrace.Vector;

public class Tree {

    // Trees have a radius, height, amount of stacked cones and position.
    // These parameters are adjustable.
    private double radius;
    private double height;
    private int amountOfCones;
    private Vector positiion;

    static private final double STEM_HEIGHT_RATIO = 0.2;    // Determines how much of the trees height consists of stem
    static private final double STEM_RADIUS_RATIO = 0.3;    // Determines how much of the trees radius conists of stem
    static private final double CONE_RATIO = 0.5;           // Determines the size of the topcone. 0.5 means the size of the top cone = 0.5 * bottom cone
    private static final double CONE_OFFSET = 0.3;          // Determines how far a cone should be places above the previous one. 0.3 means a cone will be placed 0.3 * previousCone above the previous cone

    public Tree(double radius, double height, int amountOfCones, Vector positiion) {
        this.radius = radius;
        this.height = height;
        this.amountOfCones = amountOfCones;
        this.positiion = positiion;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getAmountOfCones() {
        return amountOfCones;
    }

    public void setAmountOfCones(int amountOfCones) {
        this.amountOfCones = amountOfCones;
    }

    public Vector getPositiion() {
        return positiion;
    }

    public void setPositiion(Vector positiion) {
        this.positiion = positiion;
    }

    public void draw(GL2 gl, GLU glu, GLUT glut) {
        gl.glPushMatrix();

        // Base of the tree
        gl.glTranslatef((float) positiion.x(), (float) positiion.y(), (float) positiion.z());
        double stemRadius = radius * STEM_RADIUS_RATIO; // Determine the radius of the stem
        double stemHeight = height * STEM_HEIGHT_RATIO; // Determine the height of the stem
        Material.applyMaterialToGL(gl, Material.WOOD);
        glut.glutSolidCylinder(stemRadius, stemHeight, 100, 100); // Draw the stem of the tree

        // Leaves (cones) of the tree
        double leavesHeight = height * (1 - STEM_HEIGHT_RATIO);
        double coneRatio = Math.pow(CONE_RATIO, 1.0 / (amountOfCones - 1));
        double coneOffset = 1 + CONE_OFFSET - coneRatio;
        double bottomConeHeight = calculateBottomConeHeight(amountOfCones, coneRatio, leavesHeight, coneOffset);
        Material.applyMaterialToGL(gl, Material.GRASS);
        gl.glTranslatef(0, 0, (float) stemHeight);
        for (int i = 0; i < this.amountOfCones; i++) {
            double coneHeight = bottomConeHeight * Math.pow(coneRatio, i);
            double coneRadius = this.radius * Math.pow(coneRatio, i);
            double translation = coneHeight * coneOffset;

            glut.glutSolidCone(coneRadius, coneHeight, 100, 100);
            gl.glTranslatef(0, 0, (float) translation);
        }
        gl.glPopMatrix();
    }

    private double calculateBottomConeHeight(int amountOfCones, double coneRatio, double totalHeight, double coneOffset) {
        //Calculate the sum of the ratio's of all cones except the top cone
        double coneRatioSum = 0;
        for (int i = 0; i < amountOfCones - 1; i++) {
            coneRatioSum += Math.pow(coneRatio, i);
        }

        //Calculate the amount of times that the bottom cone will fit in the total height
        double amountOfBottomCones = coneOffset * coneRatioSum + Math.pow(coneRatio, amountOfCones - 1);

        //Calculate the height of the bottom cone
        return totalHeight / amountOfBottomCones;
    }
}
