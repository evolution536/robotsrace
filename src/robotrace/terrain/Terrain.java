package robotrace.terrain;

import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.Color;
import static java.lang.Math.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import static javax.media.opengl.GL.*;
import javax.media.opengl.GL2;
import static javax.media.opengl.GL2GL3.GL_QUADS;
import static javax.media.opengl.GL2GL3.GL_TEXTURE_1D;
import javax.media.opengl.glu.GLU;
import robotrace.Material;
import robotrace.Vector;

/**
 * Implementation of the terrain.
 */
public class Terrain {

    private Color[] texture;

    private int[] terrainSize;

    private Tree[] trees;

    private float waveIntensity;
    private float waveHeightOffset;
    private float waveMultiplier;
    private float wavePosition;
    private float waveIncreasePosition;

    public Terrain(int height, int width) {
        terrainSize = new int[]{
            height,
            width
        };

        // Create a few trees. They are drawn on the Terrain.
        trees = new Tree[]{
            new Tree(3, 13, 7, new Vector(15.0, -15.0, heightAt(15.0f, -15.0f) - 0.5f)), // Tall
            new Tree(3, 7, 3, new Vector(17.0, 17.0, heightAt(17.0f, 17.0f) - 0.5f)), // Small
            new Tree(3, 7, 3, new Vector(-12, 15.0, heightAt(-12f, 15.0f) - 0.5f)), // Small on top
            new Tree(4, 20, 10, new Vector(-17.0, -17.0, heightAt(-17.0f, -17.0f) - 0.5f)), // Mega
        };

        // Texture colors for the terrain.
        texture = new Color[4];
        texture[0] = new Color(3, 212, 253, 255); // Water
        texture[1] = new Color(193, 175, 151, 255); // Beach
        texture[2] = new Color(193, 175, 151, 255); // Beach
        texture[3] = new Color(109, 198, 41, 255); // Grass

        // Water wave parameters.
        this.wavePosition = 0.2f;
        this.waveIncreasePosition = 0.1f;
        this.waveIntensity = 0.25f;
        this.waveMultiplier = 0.25f;
        this.waveHeightOffset = 0.5f;
    }

    /**
     * Computes the elevation of the terrain at (x, y).
     */
    private float heightAt(float x, float y) {
        // We made a custom map using a custom formula!
        return (float) (0.6 * cos(0.3 * y + 0.2 * x) + 0.4 * cos(y - 0.5 * x) - (0.6 * cos(0.3 * y + 0.2 * x)))
                + (float) (0.6 * cos(0.3 * x + 0.2 * y) + 0.4 * cos(x - 0.5 * y))
                + (float) pow(x / 12, 2)
                + (float) pow(y / 12, 2)
                - 2.0f;
    }

    /**
     * Creates a 1D texture for the terrain with the specified color scheme.
     * Code taken from http://www.win.tue.nl/~vanwijk/2IV60/hints.html
     */
    private int create1DTexture(GL2 gl, Color[] colors) {
        int[] texid = new int[]{-1};
        gl.glGenTextures(1, texid, 0);
        ByteBuffer bb = ByteBuffer.allocateDirect(colors.length * 4).order(ByteOrder.nativeOrder());
        for (Color color : colors) {
            int pixel = color.getRGB();
            bb.put((byte) ((pixel >> 16) & 0xFF)); // Red component
            bb.put((byte) ((pixel >> 8) & 0xFF));  // Green component
            bb.put((byte) (pixel & 0xFF));         // Blue component
            bb.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component
        }
        bb.flip();
        gl.glBindTexture(GL_TEXTURE_1D, texid[0]);
        gl.glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        gl.glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        gl.glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA8, colors.length, 0, GL_RGBA, GL_UNSIGNED_BYTE, bb);
        gl.glBindTexture(GL_TEXTURE_1D, 0);
        return texid[0];
    }

    private float getWaveFrame(final float x, final float y) {
        // We even have moving water, so it looks like there is tide!
        return (float) (sin(this.waveIntensity * x + wavePosition) * cos(waveIntensity * y + wavePosition) * waveMultiplier) + waveHeightOffset;
    }

    /**
     * Draws the terrain.
     */
    public void draw(GL2 gl, GLU glu, GLUT glut) {
        int textureID = create1DTexture(gl, texture);
        gl.glBindTexture(GL_TEXTURE_1D, textureID);

        // Draw the mountains
        Material.applyMaterialToGL(gl, Material.WHITE);
        gl.glBegin(GL_TRIANGLES);
        for (int x = -(terrainSize[0] / 2); x < (terrainSize[0] / 2) - 1; x++) {
            for (int y = -(terrainSize[1] / 2); y < (terrainSize[1] / 2) - 1; y++) {
                // Draw a triangle like                
                //4-------3
                //|       |
                //|       |
                //1-------2
                Vector v1 = new Vector(x + 0, y + 0, this.heightAt(x + 0, y + 0)); // i, j
                Vector v2 = new Vector(x + 1, y + 0, this.heightAt(x + 1, y + 0)); // i+1, j
                Vector v3 = new Vector(x + 1, y + 1, this.heightAt(x + 1, y + 1)); // i+1, i+1
                Vector v4 = new Vector(x + 0, y + 1, this.heightAt(x + 0, y + 1)); // i, j+1

                // Calculate all the edges, needed for calculating normal vectors
                Vector edge12 = v1.subtract(v2);
                Vector edge23 = v3.subtract(v2);
                Vector edge34 = v3.subtract(v4);
                Vector edge41 = v1.subtract(v4);

                // Calculate the cross product between two edges of a triangle for normal vectors
                Vector nTop = edge41.cross(edge34);
                Vector nBottom = edge23.cross(edge12);

                // Draw BOTTOM-(RIGHT) triangle
                gl.glNormal3d(nBottom.x(), nBottom.y(), nBottom.z());
                gl.glTexCoord1d(v1.z());
                gl.glVertex3d(v1.x(), v1.y(), v1.z());
                gl.glTexCoord1d(v2.z());
                gl.glVertex3d(v2.x(), v2.y(), v2.z());
                gl.glTexCoord1d(v3.z());
                gl.glVertex3d(v3.x(), v3.y(), v3.z());

                // Draw TOP-(LEFT) triangle
                gl.glNormal3d(nTop.x(), nTop.y(), nTop.z());
                gl.glTexCoord1d(v1.z());
                gl.glVertex3d(v1.x(), v1.y(), v1.z());
                gl.glTexCoord1d(v3.z());
                gl.glVertex3d(v3.x(), v3.y(), v3.z());
                gl.glTexCoord1d(v4.z());
                gl.glVertex3d(v4.x(), v4.y(), v4.z());
            }
        }
        gl.glEnd();
        gl.glBindTexture(GL_TEXTURE_1D, 0); // Reset texture

        // Draw water across the land where z=0
        float waterHeight = getWaveFrame(-(terrainSize[0] / 2), -(terrainSize[1] / 2));
        Material.applyMaterialToGL(gl, Material.TRANSPARENT);
        gl.glBegin(GL_QUADS);
        gl.glVertex3d(-(terrainSize[0] / 2), -(terrainSize[1] / 2), waterHeight);
        gl.glVertex3d((terrainSize[0] / 2), -(terrainSize[1] / 2), waterHeight);
        gl.glVertex3d((terrainSize[0] / 2), (terrainSize[1] / 2), waterHeight);
        gl.glVertex3d(-(terrainSize[0] / 2), (terrainSize[1] / 2), waterHeight);
        gl.glEnd();
        wavePosition += waveIncreasePosition;

        // Draw cool trees!
        for (Tree t : this.trees) {
            t.draw(gl, glu, glut);
        }
    }
}
