package robotrace.robot;

import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import robotrace.Vector;

public class RobotLowerArm extends RobotLimb {

    RobotLowerArm() {
        super(new Vector(0, 0, 0), new Vector(0, 0, 0));
    }

    @Override
    protected void drawStickFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Apply some more transformation relative to the lower arm.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);

        gl.glPushMatrix();

        // Move the elbow back up a little.
        gl.glTranslatef(0.0f * scale, -0.1f * scale, 0.25f * scale);

        // Use yellow for the joints.
        gl.glColor3f(1.0f, 1.0f, 0.0f);

        // Draw a sphere for the elbow.
        glut.glutSolidSphere(0.1 * scale, 10, 8);

        // Reset the color.
        gl.glColor3f(0.0f, 0.0f, 0.0f);

        gl.glPopMatrix();

        // Draw a wired cube to make stick figure upper arms.
        glut.glutWireCube(0.6f);
        
        gl.glPopMatrix();
        gl.glPopMatrix();
    }

    @Override
    protected void drawRegularFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();
        
        // Apply some more transformation relative to the lower arm.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);
        
        // Draw a solid cube to make upper arms.
        glut.glutSolidCube(0.6f);
        
        gl.glPopMatrix();
        gl.glPopMatrix();
    }
}
