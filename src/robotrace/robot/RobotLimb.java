package robotrace.robot;

import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import robotrace.Vector;

public abstract class RobotLimb {

    Vector position;
    Vector direction;

    public RobotLimb(Vector position, Vector direction) {
        this.position = position;
        this.direction = direction;
    }

    public Vector getPosition() {
        return position;
    }

    public void setPosition(Vector position) {
        this.position = position;
    }

    public Vector getDirection() {
        return direction;
    }

    public void setDirection(Vector direction) {
        this.direction = direction;
    }

    public void draw(GL2 gl, GLU glu, GLUT glut, boolean stickFigure, float tAnim, final float scale) {
        if (stickFigure) {
            drawStickFigure(gl, glu, glut, tAnim, scale);
        } else {
            drawRegularFigure(gl, glu, glut, tAnim, scale);
        }
    }

    protected abstract void drawStickFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, final float scale);

    protected abstract void drawRegularFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, final float scale);
}
