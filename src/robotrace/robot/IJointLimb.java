/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotrace.robot;

/**
 *
 * @author ghais
 * 
 * This is an interface class that enables limbs of the robot to move.
 */
public interface IJointLimb
{
    public float getMaxRotation();
    public void setMaxRotation(final float angle);
    public float tickRotation();
    public float getCurrentRotation();
    public float getTranquilizer();
}
