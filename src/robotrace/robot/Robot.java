package robotrace.robot;

import com.jogamp.opengl.util.gl2.GLUT;
import java.util.Random;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import robotrace.Material;
import robotrace.Vector;
import robotrace.robot.RobotShoulder.RobotShoulderPosition;
import robotrace.robot.RobotTorso.RobotTorsoPosition;

/**
 * Represents a Robot, to be implemented according to the Assignments.
 */
public class Robot {

    private static final float BASE_SPEED = 0.55f;            // Use a value between 0 and 1
    private static final float VARIANCE_SPEED = 0.85f;        // Use a value between 0 and 1
    private static final float ROBOT_SIZE = 2.5f;             // Size of the robot in unit length (meters)

    /**
     * The position of the robot.
     */
    private Vector position = new Vector(0, 0, 0);

    /**
     * The position of the robot, on the track ([0,1]).
     */
    private double positionOnTrack = 0;

    /**
     * The direction in which the robot is running.
     */
    private Vector direction = new Vector(1, 0, 0);

    /**
     * The scalar for the size of the robot.
     */
    private float scale;

    /**
     * The material from which this robot is built.
     */
    private final Material material;

    /**
     * Robot body parts
     */
    private final RobotHead roboHead;
    private final RobotShoulder roboShoulderLeft, roboShoulderRight;
    private final RobotUpperArm roboUpperArmLeft, roboUpperArmRight;
    private final RobotLowerArm roboLowerArmLeft, roboLowerArmRight;
    private final RobotTorso roboTorsoTop, roboTorsoBottom;
    private final RobotUpperLeg roboUpperLegLeft, roboUpperLegRight;
    private final RobotLowerLeg roboLowerLegLeft, roboLowerLegRight;

    /**
     * Constructs the robot with given parameters.
     *
     * @param position location of the robot
     * @param direction
     * @param material material the robot is made of
     * @param scale size scalar of the robot
     */
    public Robot(Vector position, Vector direction, Material material, final float scale) {
        this.position = position;
        this.direction = direction;
        this.material = material;
        this.scale = scale;

        this.roboHead = new RobotHead();
        this.roboTorsoTop = new RobotTorso(RobotTorsoPosition.TOP);
        this.roboTorsoBottom = new RobotTorso(RobotTorsoPosition.BOTTOM);
        this.roboShoulderLeft = new RobotShoulder(RobotShoulderPosition.LEFT);
        this.roboShoulderRight = new RobotShoulder(RobotShoulderPosition.RIGHT);
        this.roboUpperArmLeft = new RobotUpperArm(false);
        this.roboUpperArmRight = new RobotUpperArm(true);
        this.roboLowerArmLeft = new RobotLowerArm();
        this.roboLowerArmRight = new RobotLowerArm();
        this.roboUpperLegLeft = new RobotUpperLeg(false);
        this.roboUpperLegRight = new RobotUpperLeg(true);
        this.roboLowerLegLeft = new RobotLowerLeg();
        this.roboLowerLegRight = new RobotLowerLeg();
        
        this.roboUpperArmLeft.setMaxRotation(40.0f);
        this.roboUpperArmRight.setMaxRotation(40.0f);
        this.roboUpperLegLeft.setMaxRotation(40.0f);
        this.roboUpperLegRight.setMaxRotation(40.0f);
    }

    /**
     * Draws this robot (as a {@code stickfigure} if specified).
     *
     * @param gl
     * @param glu
     * @param glut
     * @param stickFigure
     * @param tAnim
     */
    public void draw(GL2 gl, GLU glu, GLUT glut, boolean stickFigure, float tAnim) {
        Material.applyMaterialToGL(gl, material);

        gl.glPushMatrix();
        gl.glTranslated(position.x(), position.y(), position.z());

        //gl.glRotated(direction.x(), 1, 0, 0);
        //gl.glRotated(direction.y(), 0, 1, 0);
        gl.glRotated(direction.z(), 0, 0, 1);

        // Draw the robot head.
        roboHead.draw(gl, glu, glut, stickFigure, tAnim, scale);
        
        // Draw left shoulder and arm parts.
        roboShoulderLeft.draw(gl, glu, glut, stickFigure, tAnim, scale);
        roboUpperArmLeft.draw(gl, glu, glut, stickFigure, tAnim, scale);
        roboLowerArmLeft.draw(gl, glu, glut, stickFigure, tAnim, scale);
        
        // Draw right shoulder and arm parts.
        roboShoulderRight.draw(gl, glu, glut, stickFigure, tAnim, scale);
        roboUpperArmRight.draw(gl, glu, glut, stickFigure, tAnim, scale);
        roboLowerArmRight.draw(gl, glu, glut, stickFigure, tAnim, scale);
        
        // Draw torso parts.
        roboTorsoTop.draw(gl, glu, glut, stickFigure, tAnim, scale);
        roboTorsoBottom.draw(gl, glu, glut, stickFigure, tAnim, scale);
        
        // Draw left leg parts.
        roboUpperLegLeft.draw(gl, glu, glut, stickFigure, tAnim, scale);
        roboLowerLegLeft.draw(gl, glu, glut, stickFigure, tAnim, scale);
        
        // Draw right leg parts.
        roboUpperLegRight.draw(gl, glu, glut, stickFigure, tAnim, scale);
        roboLowerLegRight.draw(gl, glu, glut, stickFigure, tAnim, scale);
        
        gl.glPopMatrix();
    }

    /*
     * Returns the scale of the robot.
     */
    public float getScale() {
        return this.scale;
    }

    /*
     * Sets the scale of the robot.
     */
    public void setScale(final float scale) {
        this.scale = scale;
    }

    /*
     * Returns the position of the robot.
     */
    public Vector getPosition() {
        return position;
    }
    
    /*
     * Sets the position of the robot.
     */
    public void setPosition(Vector position) {
        this.position = position;

        this.roboHead.setPosition(new Vector(0f, 0f, Robot.ROBOT_SIZE));
        this.roboTorsoTop.setPosition(this.roboHead.position.subtract(new Vector(0.0f, 0.0f, 0.3f)));
        this.roboTorsoBottom.setPosition(this.roboTorsoTop.position.subtract(new Vector(0.0f, 0.0f, 1.0f)));
        this.roboShoulderLeft.setPosition(this.roboTorsoTop.position.subtract(new Vector(0.5f, 0.0f, 0.1f)));
        this.roboShoulderRight.setPosition(this.roboTorsoTop.position.subtract(new Vector(-0.5f, 0.0f, 0.1f)));
        this.roboUpperArmLeft.setPosition(new Vector(0.0f, 0.0f, -0.2));
        this.roboUpperArmRight.setPosition(new Vector(0.0f, 0.0f, -0.2));
        this.roboLowerArmLeft.setPosition(new Vector(0.0f, 0.0f, -0.5f));
        this.roboLowerArmRight.setPosition(new Vector(0.0f, 0.0f, -0.5f));
        this.roboUpperLegLeft.setPosition(this.roboTorsoBottom.position.subtract(new Vector(0.25f, 0.0f, 0.3f)));
        this.roboUpperLegRight.setPosition(this.roboTorsoBottom.position.subtract(new Vector(-0.25f, 0.0f, 0.3f)));
        this.roboLowerLegLeft.setPosition(new Vector(0.0f, 0.0f, -0.6f));
        this.roboLowerLegRight.setPosition(new Vector(0.0f, 0.0f, -0.6f));
    }

    /*
     * Returns the direction of the robot.
     */
    public Vector getDirection() {
        return direction;
    }
    
    /*
     * Sets the direction of the robot.
     */
    public void setDirection(Vector direction, Vector direction2) {
        this.direction = new Vector(direction.x(), direction.y(), (float) Math.toDegrees(Math.atan2(direction.y(), direction.x()) - Math.atan2(direction2.y(), direction2.x())));
    }
    
    /*
     * Returns the position of the robot on the racetrack.
     */
    public double getPositionOnTrack() {
        return positionOnTrack;
    }
    
    /*
     * Sets the position of the robot on the racetrack.
     */
    public void setPositionOnTrack(double positionOnTrack) {
        this.positionOnTrack = positionOnTrack;
    }
    
    /*
     * Moves the robot on the racetrack. Each robot has a random movement offset.
     */
    public void walk() {
        double baseSpeed = (double) Robot.BASE_SPEED * 100; // Scale the base speed
        double variationSpeed = (double) new Random().nextInt((int) (Robot.VARIANCE_SPEED * 100) + 1); // Scale the variation speed

        double increaseInTrackPosition = baseSpeed + variationSpeed; // Generate a speed for this robo to move!
        double newTrackPosition = (this.getPositionOnTrack() + increaseInTrackPosition / 100000); // Add the increase in position

        this.setPositionOnTrack(newTrackPosition % 1); // Update the robo's position on track   }

        //move();
    }

    /*private void move() {
        Random r = new Random();

        this.roboUpperArmLeft.direction = new Vector(r.nextInt(45), 0, 0);
        this.roboUpperArmRight.direction = new Vector(r.nextInt(45), 0, 0);

        this.roboUpperLegLeft.direction = new Vector(r.nextInt(45), 0, 0);
        this.roboUpperLegRight.direction = new Vector(r.nextInt(45), 0, 0);
    }*/
}
