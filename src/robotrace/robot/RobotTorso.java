package robotrace.robot;

import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import robotrace.Vector;

public class RobotTorso extends RobotLimb {

    public enum RobotTorsoPosition {
        TOP, BOTTOM,
    }

    private RobotTorsoPosition robotTorsoPosition;

    RobotTorso(RobotTorsoPosition robotTorsoPosition) {
        super(new Vector(0, 0, 0), new Vector(0, 0, 0));
        this.robotTorsoPosition = robotTorsoPosition;
    }

    @Override
    protected void drawStickFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Translate to the origin of the preceding limb.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);

        // Scale the limb according to the specified scaling factor.
        gl.glScalef(scale, scale, scale);

        if (this.robotTorsoPosition == RobotTorsoPosition.TOP) {
            gl.glPushMatrix();

            // Draw a connecting line between the two shoulders.
            gl.glTranslatef(0.0f, 0.0f, -0.1f);
            gl.glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
            gl.glScalef(0.1f, 0.1f, 1.0f);
            glut.glutWireCube(0.8f);

            gl.glPopMatrix();

            gl.glPushMatrix();

            // The cone for the regular robot torso requires a little different
            // translation. We use special translation to position the stick 
            // figure properly.
            gl.glTranslatef(0.0f, 0.0f, -0.5f);

            // Scale the torso down even more to make it look like a stick figure.
            gl.glScalef(0.1f, 0.1f, 1.0f);

            // Draw the upper torso stick figure using a wired scaled cube.
            glut.glutWireCube(1.0f);

            gl.glPopMatrix();

            gl.glPushMatrix();

            // Draw a connecting line between the hips.
            gl.glTranslatef(0.0f, 0.0f, -1.0f);
            gl.glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
            gl.glScalef(0.1f, 0.1f, 1.0f);

            // Adjust to the rotation of the limb
            gl.glRotated(this.direction.x(), 1, 0, 0);
            gl.glRotated(this.direction.y(), 0, 1, 0);
            gl.glRotated(this.direction.z(), 0, 0, 1);

            glut.glutWireCube(0.6f);
            gl.glPopMatrix();
        }
        
        gl.glPopMatrix();
    }

    @Override
    protected void drawRegularFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Translate to the origin of the preceding limb.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);

        // Scale the limb according to the specified scaling factor.
        gl.glScalef(scale, scale, scale);

        if (this.robotTorsoPosition == RobotTorsoPosition.TOP) {
            // Rotate the cone to be upside down.
            gl.glRotatef(180.0f, 1.0f, 0.0f, 0.0f);

            // Draw the lower torso using a bottom-up cone.
            glut.glutSolidCone(0.5, 1.0, 20, 20);
        }

        // Adjust to the rotation of the limb
        gl.glRotated(this.direction.x(), 1, 0, 0);
        gl.glRotated(this.direction.y(), 0, 1, 0);
        gl.glRotated(this.direction.z(), 0, 0, 1);

        // Draw the lower torso using a bottom-up cone.
        glut.glutSolidCone(0.3, 1.0, 20, 20);
        
        gl.glPopMatrix();
    }
}
