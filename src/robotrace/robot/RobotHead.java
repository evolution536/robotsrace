package robotrace.robot;

import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import robotrace.Vector;

public class RobotHead extends RobotLimb {

    public RobotHead() {
        super(new Vector(0, 0, 0), new Vector(0, 0, 0));
    }
    
    @Override
    protected void drawStickFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Transform to the correct position.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);

        // Scale the limb according to the specified scaling factor.
        gl.glScalef(scale, scale, scale);

        // Adjust to the rotation of the limb
        gl.glRotated(this.direction.x(), 1, 0, 0);
        gl.glRotated(this.direction.y(), 0, 1, 0);
        gl.glRotated(this.direction.z(), 0, 0, 1);

        glut.glutWireSphere(0.3, 25, 20);
        
        gl.glPopMatrix();
    }

    @Override
    protected void drawRegularFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Transform to the correct position.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);

        // Scale the limb according to the specified scaling factor.
        gl.glScalef(scale, scale, scale);

        // Adjust to the rotation of the limb
        gl.glRotated(this.direction.x(), 1, 0, 0);
        gl.glRotated(this.direction.y(), 0, 1, 0);
        gl.glRotated(this.direction.z(), 0, 0, 1);
        
        // Draw the head of the robot.
        glut.glutSolidSphere(0.3, 25, 20);
        
        gl.glPopMatrix();
    }
}
