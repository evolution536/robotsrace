package robotrace.robot;

import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import robotrace.Vector;

public class RobotShoulder extends RobotLimb {

    public enum RobotShoulderPosition {
        LEFT, RIGHT,
    }

    private final RobotShoulderPosition robotShoulderPosition;

    RobotShoulder(RobotShoulderPosition robotShoulderPosition) {
        super(new Vector(0, 0, 0), new Vector(0, 0, 0));
        this.robotShoulderPosition = robotShoulderPosition;
    }

    @Override
    protected void drawStickFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Transform to the correct position.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);
        
        // Scale the limb according to the specified scaling factor.
        gl.glScalef(scale, scale, scale);

        // Draw a wired sphere as shoulder.
        glut.glutSolidSphere(0.2, 12, 8);
    }

    @Override
    protected void drawRegularFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();
        
        // Transform to the correct position.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);
        
        // Scale the limb according to the specified scaling factor.
        gl.glScalef(scale, scale, scale);

        // Draw a solid sphere as shoulder. SHOULDERS LIKE BOULDERS!
        glut.glutSolidSphere(0.2, 12, 8);
    }
}
