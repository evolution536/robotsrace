package robotrace.robot;

import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import robotrace.Vector;

public class RobotUpperLeg extends RobotLimb implements IJointLimb {

    float maxRotation = 0.0f;
    float rotationCounter = 0.0f;
    boolean goingUp = true;
    boolean orientation = false; // false = left, true = right.
    
    RobotUpperLeg(final boolean orientation) {
        super(new Vector(0, 0, 0), new Vector(0, 0, 0));
        this.orientation = orientation;
    }

    @Override
    protected void drawStickFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Transform to the correct position.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);

        gl.glPushMatrix();

        // Move the hip joints back up a little.
        gl.glTranslatef(0.0f * scale, 0.0f * scale, 0.3f * scale);

        // Draw a sphere for the hip joints.
        glut.glutSolidSphere(0.1 * scale, 10, 8);
        
        gl.glPopMatrix();
        
        // Adjust to the rotation of the limb
        gl.glRotated(this.direction.x(), 1, 0, 0);
        gl.glRotated(this.direction.y(), 0, 1, 0);
        gl.glRotated(this.direction.z(), 0, 0, 1);
        
        gl.glRotatef(this.orientation ? this.tickRotation() : -this.tickRotation(), 1.0f, 0.0f, 0.0f);

        // Scale the upper arms to have them look long.
        gl.glScalef(0.25f, 0.25f, 1.0f);

        // Scale the limb according to the specified scaling factor.
        gl.glScalef(scale, scale, scale);

        // Scale the lower leg down even more to make it look like a stick figure.
        gl.glScalef(0.2f, 0.2f, 1.0f);

        // Draw a wired cube to make stick figure upper arms.
        glut.glutWireCube(0.6f);
    }

    @Override
    protected void drawRegularFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Transform to the correct position.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);

        // Adjust to the rotation of the limb
        gl.glRotated(this.direction.x(), 1, 0, 0);
        gl.glRotated(this.direction.y(), 0, 1, 0);
        gl.glRotated(this.direction.z(), 0, 0, 1);
        
        gl.glRotatef(this.orientation ? this.tickRotation() : -this.tickRotation(), 1.0f, 0.0f, 0.0f);
        
        // Scale the upper legs to have them look long.
        gl.glScalef(0.25f, 0.25f, 1.0f);

        // Scale the limb according to the specified scaling factor.
        gl.glScalef(scale, scale, scale);

        // Draw a solid cube to make upper arms.
        glut.glutSolidCube(0.6f);
    }
    
    @Override
    public void setMaxRotation(float angle)
    {
        this.maxRotation = angle;
    }

    @Override
    public float tickRotation()
    {
        if (this.goingUp)
        {
            final float retVal = this.maxRotation * (float)Math.sin(this.rotationCounter++ * this.getTranquilizer());
            if (this.rotationCounter >= this.maxRotation)
            {
                this.goingUp = !this.goingUp;
            }
            return retVal;
        }
        else
        {
            final float retVal = -this.maxRotation * (float)Math.sin(this.rotationCounter-- * this.getTranquilizer());
            if (this.rotationCounter <= -this.maxRotation)
            {
                this.goingUp = !this.goingUp;
            }
            return retVal;
        }
    }

    @Override
    public float getCurrentRotation()
    {
        return this.rotationCounter;
    }

    @Override
    public float getTranquilizer()
    {
        // A lower value gives a slower animation!
        return 0.3f;
    }

    @Override
    public float getMaxRotation()
    {
        return this.maxRotation;
    }
}
