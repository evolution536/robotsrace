package robotrace.robot;

import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import robotrace.Vector;

public class RobotLowerLeg extends RobotLimb {

    RobotLowerLeg() {
        super(new Vector(0, 0, 0), new Vector(0, 0, 0));
    }

    @Override
    protected void drawStickFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Transform to the correct position.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);

        gl.glPushMatrix();

        // Move the knee back up a little.
        gl.glTranslatef(0.0f * scale, 0.0f * scale, 0.3f * scale);

        // Draw a sphere for the knee.
        glut.glutSolidSphere(0.1 * scale, 10, 8);

        gl.glPopMatrix();

        // Adjust to the rotation of the limb
        gl.glRotated(this.direction.x(), 1, 0, 0);
        gl.glRotated(this.direction.y(), 0, 1, 0);
        gl.glRotated(this.direction.z(), 0, 0, 1);

        // Draw a wired cube to make stick figure upper arms.
        glut.glutWireCube(0.6f);
        
        gl.glPopMatrix();  
        gl.glPopMatrix();
    }

    @Override
    protected void drawRegularFigure(GL2 gl, GLU glu, GLUT glut, float tAnim, float scale) {
        gl.glPushMatrix();

        // Transform to the correct position.
        gl.glTranslated(this.position.x() * scale, this.position.y() * scale, this.position.z() * scale);

        // Adjust to the rotation of the limb
        gl.glRotated(this.direction.x(), 1, 0, 0);
        gl.glRotated(this.direction.y(), 0, 1, 0);
        gl.glRotated(this.direction.z(), 0, 0, 1);
        
        // Draw a solid cube to make upper arms.
        glut.glutSolidCube(0.6f);

        gl.glPopMatrix();
        gl.glPopMatrix();
    }
}
